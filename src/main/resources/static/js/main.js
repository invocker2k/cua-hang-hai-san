$(document).ready(function(){
    updateCart()
});

let listProducts;

let tabContainer = document.getElementById('tab-container')
//GET DATA 
let danhmuc;
async function getData() {
    danhmuc = await axios.get('/api/v1/dm')
    danhmuc = danhmuc.data
    listProducts = await getSp()
    listProducts=  listProducts.data

    //Hiển thị danh mục va tab danh muc
    let nav = document.getElementById('nav')
    let tabdanhmuc = document.getElementsByClassName('tabdanhmuc')
    // 1- tab danh muc
    let content_tabdanhmuc = `
                        <div class="navbar nav-menu">
                            <div class="navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <div class="new_title">
                                            <h2>New Products</h2>
                                        </div>
                                    </li>
                           
    `
    for (let i = 0; i < danhmuc.length; i++) {
        content_tabdanhmuc += i==0? ` <li class="active">
        <a data-toggle="tab" href="#tab1" class="">${danhmuc[i].tendanhmuc}</a>
        </li>`:`
        <li><a data-toggle="tab" href="#tab${i+1}" class="">${danhmuc[i].tendanhmuc}</a> </li>            
        `
    }
    content_tabdanhmuc +=`</ul></div>`
    if (tabdanhmuc[0] != null){ tabdanhmuc[0].innerHTML = content_tabdanhmuc}

    // 2 - category from menu
    let list_category = `<li class="level0 parent drop-menu" id="nav-home" ><a href="/" class="level-top"><i
                                    class="fa fa-home"></i><span class="hidden">Home</span></a>
                                <ul style="display: none;" class="level1">
                                  <li class="level1"><a href="/"><span>Main Store</span></a> </li>
                                  <li class="level1"><a href="/login"><span>Login</span></a> </li>
                                  <li class="level1"><a href="/admin"><span>admin</span></a> </li>
                                  <li class="level1"><a href="/shoppingcart"><span>shoppingcart</span></a> </li>

                                </ul>
                        `

    for (let i = 0; i < danhmuc.length; i++) {
        list_category += `<li class="level0 nav-6 level-top drop-menu"> <a class="level-top" href="#tab${i}">
                        <span > ${danhmuc[i].tendanhmuc}</span> </a>
                        <ul class="level1" style="display: none;">
                     `

        list_category += addSpToMenu(await getSpByMaDm(danhmuc[i].id))
        list_category += `  
            </ul>
</li>`}
    function addSpToMenu(listProductsByCategory){
        let sp =``

        listProductsByCategory.forEach((val)=>{
            sp += `
                        <li class="level2 first">
                            <a href="/sp/detail/${val.masanpham}"><span>${val.tensanpham}
                            </span></a>
                        </li>
        `;
        })
        return sp
    }


    nav.innerHTML = list_category



    //Done hiển thị danh mục



    // List products
    let content = '';
    content += ` <div class="tab-panel active" id="tab${danhmuc[0].id}">
                    <div class="category-products">
                        <ul id='list1' class="products-grid">
                        </ul>
                        </div>
                    </div>`

    for (let i = 1; i < danhmuc.length; i++) {
        content += ` <div class="tab-panel" id="tab${danhmuc[i].id}">
                    <div class="category-products">
                        <ul id='list${danhmuc[i].id}' class="products-grid">
                        </ul>
                    </div>
                </div>`

    }
    if (tabContainer != null){ tabContainer.innerHTML = content; }
    for (let i = 0; i < danhmuc.length; i++) {

        document.getElementById('list' + danhmuc[i].id).innerHTML = showProduct(listProducts.filter(x => x.madanhmuc == danhmuc[i].id))

    }
    //dONE tab DANH MUC
    //BEST SELLER
    let bestseller = document.getElementById('bestseller')
    if (bestseller != null){
        bestseller.innerHTML = showlist(listProducts.slice(0,7))
    }

    /* Bestsell slider */
    jQuery("#bestsell-slider .slider-items").owlCarousel({
        items: 4, //10 items above 1000px browser width
        itemsDesktop: [1024, 3], //5 items between 1024px and 901px
        itemsDesktopSmall: [900, 2], // 3 items betweem 900px and 601px
        itemsTablet: [768, 2], //2 items between 600 and 0;
        itemsMobile: [360, 1],
        navigation: true,
        navigationText: ["<a class=\"flex-prev\"></a>", "<a class=\"flex-next\"></a>"],
        slideSpeed: 500,
        pagination: false
    });
}
getData()



//CART
let cartSidebar = document.getElementById('cart-sidebar') //giỏ hàng
let cartCount = document.getElementById('cart_count')
function find(id, list) {
    return list.find((x) => x.masanpham == id)
}
let listCarts =[]
// add cart
function addCart(masp){
    let finditem = find(masp, listProducts)
    $.ajax({

        url:"/api/v1/gh",
        type:"post",
        data:{
            ma_sp: finditem.masanpham,
            so_luong: 1,
            ten_sp: finditem.tensanpham,
            gia_tien: finditem.giatien,
            ma_chi_tiet_sp: finditem.madanhmuc,
            url_img_sp: finditem.hinhsanpham
        },
        success:function(value){
            listCarts = value
            showCart(listCarts)

        }
    })
    alert("Bạn vừa thêm sản phẩm, Xem giỏ hàng nhé!")
}


// show cart
function showCart(list) {
    let count = 0;
    let content = '';
    if (list.length == 0) { content = 'Chưa có sản phẩm nào trong giỏ' }
    for (let i = 0; i < list.length; i++) {
        count = count + list[i].so_luong
        content += `
        <li class="item ">
        <div class="item-inner"> <a class="product-image"
                title=${list[i].tenSP} href="#"><img alt=${list[i].tenSP}
                    src="/products-images/${list[i].imgUrlSP}" > </a>
            <div class="product-details">
               <a class="btn-remove1"
                        title="Remove This Item" onclick="removeItem(event)"  masp=${list[i].masp} >Remove</a> 
                <strong>${list[i].so_luong}</strong> x <span
                    class="price">${list[i].giaTien} đ</span>
                <p class="product-name"><a href="/sp/detail/${list[i].masp}">${list[i].tenSP}</a>
                </p>
            </div>
        </div>
    </li>`
    }
    cartSidebar.innerHTML = content
    cartCount.innerHTML = count
}
let addtoCart = document.getElementsByClassName('btn-cart')
// Remove Cart
function removeItem(e) {
    let buttonClicked = e.target;
    buttonClicked.parentElement.parentElement.parentElement.remove();
    masanpham = $(buttonClicked).attr("masp")
    removecartAPI(masanpham)
    //  updateCartTotal();
}
//update cart
function updateCart() {
    $.ajax({
        url:"/api/v1/gh",
        type:"get",
        success:function(value){
            listCarts = value
            showCart(listCarts)
        }
    })
}
// delete cart api
function removecartAPI(masanpham){
    $.ajax({
        url:"/api/v1/gh",
        type:"delete",
        data:{
            ma_sp: masanpham,
        },
        success:()=>{
            updateCart()
            updateCartProducts();
        }
    })
}
/*  */
// Show product
function showProduct(products) {
    let content = '';
    for (let i = 0; i < products.length; i++) {
        content += `
    <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
    <div class="item-inner">
        <div class="item-img">
            <div class="item-img-info"> <a class="product-image"
                    title=${products[i].tensanpham}
                    href="/sp/detail/${products[i].masanpham}"> <img
                        alt=${products[i].tensanpham}
                        src=/products-images/${products[i].hinhsanpham}> </a>
                <div class="box-hover">
                    <ul class="add-to-links">
                        <li><a class="link-quickview"
                                href="#">Quick
                                View</a>
                        </li>
                        <li><a class="link-wishlist"
                                href="#">Wishlist</a>
                        </li>
                        <li><a class="link-compare"
                                href="#">Compare</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="item-info">
            <div class="info-inner">
                <div class="item-title"> <a
                        title=${products[i].tensanpham}
                        href="/sp/detail/${products[i].masanpham}"> ${products[i].tensanpham}
                    </a> </div>
                <div class="item-content">
                    <div class="item-price">
                        <div class="price-box"> <span
                                class="regular-price"> <span
                                    class="price">${products[i].giatien} <u>đ</u></span>
                            </span>
                        </div>
                    </div>
                    <div class="action">
                        <button class="button btn-cart"
                            type="button" title=""
                            data-original-title="Add to Cart"  onclick='addCart( ${products[i].masanpham})'><span>Thêm vào giỏ hàng</span> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>`
    }
    return content
}

//Show list
function showlist(list) {
    let content = '';
    list.forEach(product =>{
        content += ` 
      <div class="item">
      <div class="item-inner">
        <div class="item-img">
          <div class="item-img-info">
            <a class="product-image" title="${product.tensanpham}" href="/sp/detail/${product.masanpham}"> <img
                alt="${product.tensanpham}" src="/products-images/${product.hinhsanpham}"> </a>
            <div class="box-hover">
              <ul class="add-to-links">
                <li><a class="link-quickview" href="">Quick View</a>
                </li>
                <li><a class="link-wishlist" href="">Wishlist</a>
                </li>
                <li><a class="link-compare" href="">Compare</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item-info">
          <div class="info-inner">
            <div class="item-title"> <a title="${product.tensanpham}" href="/sp/detail/${product.masanpham}"> ${product.tensanpham} </a> </div>
           
            <div class="item-content">
              <div class="item-price">
                <div class="price-box"> <span class="regular-price"> <span class="price">${product.giatien} <u>đ</u></span>
                  </span>
                </div>
              </div>
              <div class="action">
                <button class="button btn-cart" type="button" title=""
                  data-original-title="Add to Cart"><span>Thêm vào giỏ hàng</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>`
    })

    return content
}

function veTrangChu(){
    location.assign("/");
}
let formData = new FormData();
let imagefile = document.querySelector('#InputFile');

formData.append("file", imagefile.files[0]);

$("#InputFile").change(()=>{
   async function f() {
      await axios.post('/file/only', formData, {
           headers: {
               'Content-Type': 'multipart/form-data'
           }
       })
   }
   f()
})
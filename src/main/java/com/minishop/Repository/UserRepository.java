package com.minishop.Repository;

import com.minishop.Model.d_User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<d_User,Long>{
    d_User findByUsername(String name);
}
package com.minishop.Repository;

import com.minishop.Model.CustomerReview;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerReviewRepository extends CrudRepository<CustomerReview, Integer> {
    @Query("from customer_review where id_product = ?1 order by time_review")
    List<CustomerReview> findAllById_product_detail(int idProductDetail);
}

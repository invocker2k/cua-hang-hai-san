package com.minishop.Repository;

import com.minishop.Model.ChiTietSanPham;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ChiTietSanPhamRepository extends JpaRepository<ChiTietSanPham, Integer>{

}
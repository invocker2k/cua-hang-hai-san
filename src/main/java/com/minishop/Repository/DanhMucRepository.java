package com.minishop.Repository;

import java.util.List;

import com.minishop.Model.DanhMucSanPham;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DanhMucRepository extends JpaRepository<DanhMucSanPham,Integer>{
@Query(" from danhmucsanpham")
List<DanhMucSanPham> getAllDanhMuc();
}
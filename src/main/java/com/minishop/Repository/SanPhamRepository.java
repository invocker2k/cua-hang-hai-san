package com.minishop.Repository;

import java.util.List;

import com.minishop.Model.SanPham;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SanPhamRepository extends JpaRepository<SanPham,Integer>{
    List<SanPham> findByMadanhmuc(int madanhmuc);
}
package com.minishop.Repository;

import com.minishop.Model.HoaDon;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HoaDonRepository extends JpaRepository<HoaDon,Integer>{

}
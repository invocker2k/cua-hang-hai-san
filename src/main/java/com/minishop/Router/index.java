package com.minishop.Router;

import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpSession;

import com.minishop.Model.SanPham;
import com.minishop.Repository.SanPhamRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/")
public class index {
        @Autowired
        SanPhamRepository sanPhamR;

        @GetMapping()
    public String indexApp ( ){
            return "index";
}
        @GetMapping("sp/detail/{id}")
        public String detailPage (@PathVariable int id,ModelMap mm){
        SanPham sp = sanPhamR.findById(id).orElse(null);
           mm.addAttribute("spRes", sp);
                return "detail";
        }
        @GetMapping("test/detail")
        public String detailPageTest (){
                return "detail";
        }
        @GetMapping("login")
        public String loginPage (){
                return "login";
        }
        @GetMapping(value="shoppingcart")
        public String getMethodName() {
            return "shopping_cart";
        }

        @GetMapping("sp/dm/{id}")
        public String findSpfromDanhMuc (@PathVariable int id){
        SanPham sp = sanPhamR.findById(id).orElse(null);
                return "category_product";
        }



        
}
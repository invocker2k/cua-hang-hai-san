package com.minishop.Router;

import com.minishop.Model.SanPham;
import com.minishop.Repository.SanPhamRepository;
import com.minishop.services.uploads.StorageProperties;
import com.minishop.services.uploads.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/sp")
public class Product_management {

    private  StorageService storageService;
    @Autowired
    StorageProperties storageUrl;

    @Autowired
    SanPhamRepository sanPhamR;

    @PostMapping
    public String addSanPham(@RequestParam("tensanpham") String name,
                             @RequestParam("giatien") String giatien,
                             @RequestParam("mota") String mota,
                             @RequestParam("madanhmuc") int danhmuc,
                             @RequestParam("hinhsanpham") String url){

      SanPham sp = new SanPham();
      sp.setMota(mota);sp.setGiatien(giatien);sp.setMadanhmuc(danhmuc);sp.setTensanpham(name);
      sp.setHinhsanpham(url);
        sanPhamR.save(sp);
        return "/admin/product_management";
    }


}

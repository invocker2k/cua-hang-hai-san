package com.minishop.Router;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
@RequestMapping("/admin")
public class admin {

    @GetMapping("/product_management")
    public String product_management(){
        return "admin/product_management";
    }

    @GetMapping()
    public String index(){
        return "admin/index";
    }

    @GetMapping("/category_management")
    public String category_management(){
        return "admin/category_management";
    }

}
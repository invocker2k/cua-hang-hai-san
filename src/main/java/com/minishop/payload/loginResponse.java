package com.minishop.payload;

import lombok.Data;

@Data
public class loginResponse {
    private String accessToken;
    private String tokenType = "Bearer";

    public loginResponse(String accessToken) {
        this.accessToken = accessToken;
    }
}
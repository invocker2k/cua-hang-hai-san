package com.minishop.payload;

import lombok.Data;

@Data
public class LoginRequest {
    
    private String username;
    private String pass;

}
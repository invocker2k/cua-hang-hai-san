package com.minishop.Model;

import java.io.Serializable;

import lombok.Data;

@Data
public class GioHang implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 3881058354275012278L;
	private int maChiTietSP;
	private String tenSP;
	private String giaTien;
	private int masp;
	private int so_luong;
	private String imgUrlSP;
}

package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class ChiTietHoaDonId implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private int mahoadon;
    private int machitietsanpham;
}
package com.minishop.Model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorsMessage {
    private int statusCode;
    private String message;
}
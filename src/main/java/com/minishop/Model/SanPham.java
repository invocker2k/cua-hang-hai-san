package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@Entity(name = "sanpham")
public class SanPham implements Serializable{
    /**
     *
     */
    public SanPham(){}
    public SanPham(int dm,String name, String gia, String mota,String linkhinh){
        this.giatien=gia;this.mota=mota;this.tensanpham = name;this.madanhmuc=dm;this.hinhsanpham = linkhinh;
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int masanpham;
	private String tensanpham;
	private String giatien;
	private String mota;
    private String hinhsanpham;
	private int madanhmuc;
}
package com.minishop.Model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "danhmucsanpham")
public class DanhMucSanPham implements Serializable{
    /**
    *
    */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String madanhmuc;
	private String tendanhmuc;
	private String hinhdanhmuc;
    
}
package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity(name = "nhanvien")
public class NhanVien implements Serializable{
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int manhanvien;
	private String hoten;
	private String diachi;
	private boolean gioitinh;
	private String cmnd;
	private String tendangnhap;
	private String matkhau;
	private String email;
}
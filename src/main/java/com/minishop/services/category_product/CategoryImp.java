/* package com.minishop.services.category_product;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


import com.minishop.Model.DanhMucSanPham;
import com.minishop.Repository.DanhMucRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
@Service
public class CategoryImp implements CategoryServices {
    @Autowired
    DanhMucRepository danhmucR;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public DanhMucSanPham findDanhMucSanPhamById(int id) {
        final String key = "category_"+id;
        final ValueOperations<String, DanhMucSanPham> operation = redisTemplate.opsForValue();
        final boolean haskey = redisTemplate.hasKey(key);
        if(haskey){
            final DanhMucSanPham dm = operation.get(key);
            return dm;
        }
       final Optional<DanhMucSanPham> danhmuc = danhmucR.findById(id);
       if(danhmuc.isPresent()){
           operation.set(key, danhmuc.get(), 10, TimeUnit.SECONDS);
           return danhmuc.get();
       }else{
           return null;
       }
    }

    @Override
    public List<DanhMucSanPham> getAllDanhMucSanPham() {
        
        return null;
    }
    
} */
package com.minishop.services;

import com.minishop.Model.CustomUserDetails;
import com.minishop.Model.d_User;
import com.minishop.Repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userR;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        d_User user = userR.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(user);
    }
    @Transactional
    public UserDetails loadUserById(Long id) {
        d_User user = userR.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );

        return new CustomUserDetails(user);
    }

}
package com.minishop.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.minishop.Model.GioHang;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@RestController
@RequestMapping("/api/v1/gh")
@SessionAttributes("GIOHANG")
public class GioHangController {
	//add
	public static final String GIO_HANG_SESSION = "GIOHANG";
    @PostMapping()
    @SuppressWarnings("unchecked")
	public List<GioHang> add_gio_hang(@RequestParam int so_luong,@RequestParam String ten_sp,@RequestParam String gia_tien,@RequestParam int ma_chi_tiet_sp,@RequestParam int ma_sp,
	 
	@RequestParam String url_img_sp, HttpSession session) {
        
		if(null==session.getAttribute(GIO_HANG_SESSION)) {
		
			List<GioHang> gioHangs=new ArrayList<GioHang>();
			GioHang gioHang=new GioHang();
			gioHang.setGiaTien(gia_tien);
			gioHang.setTenSP(ten_sp);
			gioHang.setMaChiTietSP(ma_chi_tiet_sp);
			gioHang.setMasp(ma_sp);
			gioHang.setImgUrlSP(url_img_sp);
			gioHang.setSo_luong(1);
			gioHangs.add(gioHang);
			session.setAttribute(GIO_HANG_SESSION, gioHangs);
			
		}
		else {
			List<GioHang> sessionOld=(List<GioHang>) session.getAttribute(GIO_HANG_SESSION);

			int viTri=KiemTraSanPhamCoTrongGioHang(session,ma_chi_tiet_sp,ma_sp);
			if(viTri==-1) {
				System.out.println("s p ko co trong gio hang");
				GioHang gioHang=new GioHang();
				gioHang.setGiaTien(gia_tien);
				gioHang.setTenSP(ten_sp);
				gioHang.setMasp(ma_sp);
				gioHang.setSo_luong(1);
				gioHang.setMaChiTietSP(ma_chi_tiet_sp);
				gioHang.setImgUrlSP(url_img_sp);

				sessionOld.add(gioHang);
			}
			else {
				int soLuongMoi=sessionOld.get(viTri).getSo_luong()+1;
				sessionOld.get(viTri).setSo_luong(soLuongMoi);
			}
			List<GioHang> test=(List<GioHang>) session.getAttribute(GIO_HANG_SESSION);
			for (GioHang gioHang : test) {
				System.out.println(gioHang.getTenSP()+" - "+gioHang.getSo_luong());
			}
			
		}
		List<GioHang> giohangx=(List<GioHang>) session.getAttribute(GIO_HANG_SESSION);
		return giohangx;
    }
    //get_all
    @GetMapping()
	public List<GioHang> GioHangs(HttpSession httpsession) {
		if(httpsession.getAttribute(GIO_HANG_SESSION)!=null) {
			List<GioHang> listGioHang =(List<GioHang>)httpsession.getAttribute(GIO_HANG_SESSION);
			return listGioHang;
		}
		return null;
	}
    //update_so_luong_gio _hang
    @PutMapping()
	@ResponseBody
	public void CatNhatGioHang(@RequestParam int so_luong,@RequestParam String ten_sp,@RequestParam String gia_tien,@RequestParam int ma_chi_tiet_sp,@RequestParam int ma_sp, HttpSession session) {
		if(session.getAttribute(GIO_HANG_SESSION)!=null) {
		
			List<GioHang>  sessionOld=(List<GioHang>) session.getAttribute(GIO_HANG_SESSION);
			
			for (int i = 0; i < sessionOld.size(); i++) {
				if( sessionOld.get(i).getMasp()==ma_sp) {
					sessionOld.get(i).setSo_luong(so_luong);
                    if(sessionOld.get(i).getSo_luong()!=so_luong)
                     {break;}
					
				}
			}
		}
		
    }
    //delete
    @DeleteMapping()
	public int DeleteSP(HttpSession sesion,@RequestParam int ma_sp) {
		List<GioHang> sessionOld=(List<GioHang>) sesion.getAttribute(GIO_HANG_SESSION);
		System.out.println("so luong truoc xoa:"+sessionOld.size()+"\n");
		for (int i = 0; i < sessionOld.size(); i++) {
			System.out.println("sten"+sessionOld.get(i).getTenSP());
			
			if(sessionOld.get(i).getMasp()==ma_sp) {
					sessionOld.remove(i);
					System.out.println("so luong sau khi xoa:"+sessionOld.size()+"\n");

			}
		}
		return sessionOld.size();
	}

    // kiem tra
	private int KiemTraSanPhamCoTrongGioHang(HttpSession sesion,int machitietSP,int maSP) {
		List<GioHang> sessionOld=(List<GioHang>) sesion.getAttribute(GIO_HANG_SESSION);
		for (int i = 0; i < sessionOld.size(); i++) {
			// System.out.println("\n ma sp"+sessionOld.get(i).getMasp()+" - "+maSP+"\n");
			if( sessionOld.get(i).getMaChiTietSP()==machitietSP&&sessionOld.get(i).getMasp()==maSP) {
				return i;
			}
		}
		return -1;
	}
}
package com.minishop.Controller;

import com.minishop.Model.CustomerReview;
import com.minishop.Repository.CustomerReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/review")
public class CustomerReviewController {
    @Autowired
    CustomerReviewRepository crR;
    @PostMapping
    public boolean customerReview(@RequestBody CustomerReview cr){

        try{
            crR.save(cr);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @GetMapping("/{id}")
    public List<CustomerReview> getAllReviewBySpDetail(@PathVariable int id){
        if (crR.findAllById_product_detail(id) == null){
            System.out.println(" review is null");
            return null;

        }
        return crR.findAllById_product_detail(id);

    }
}

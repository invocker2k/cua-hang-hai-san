package com.minishop.Controller;

import java.util.List;

import com.minishop.Model.d_User;
import com.minishop.Repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/admin/user")
@RestController
public class UserController {
    @Autowired
    UserRepository userR;
    @PostMapping()
    public d_User createrUser(@ModelAttribute d_User user ){
            return userR.save(user);
    }
    @GetMapping()
    public List<d_User> getAllUser( ){
            return userR.findAll();
    }
}
package com.minishop.Controller;


import java.util.List;

import com.minishop.Model.DanhMucSanPham;
import com.minishop.Repository.DanhMucRepository;

// import com.minishop.services.category_product.CategoryImp;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
@RequestMapping("/api/v1/dm")
public class danh_mucController {
    @Autowired
    DanhMucRepository danhMucR;
    // @Autowired
    // CategoryImp categoryI;
    @GetMapping
    // @Cacheable("AllDanhMuc")
    public List<DanhMucSanPham> AllDanhMuc() {
        return danhMucR.findAll();
    }
    @GetMapping("/{id}")
    public DanhMucSanPham getSingalDanhMuc(@PathVariable int id) {
    return danhMucR.getOne(id);
    }
    @PostMapping()
    public boolean postMethodName(@RequestBody DanhMucSanPham dmReq) {
       try {
           danhMucR.save(dmReq);
           return true;
       }catch (Exception e){
           return false;
       }
    }
    
    @PutMapping()
    public DanhMucSanPham putMethodName(@ModelAttribute DanhMucSanPham dmReq) {
        DanhMucSanPham dm = danhMucR.findById(dmReq.getId()).orElse(dmReq);
        dm.setHinhdanhmuc(dmReq.getHinhdanhmuc());
        dm.setMadanhmuc(dmReq.getMadanhmuc());
        dm.setTendanhmuc(dmReq.getTendanhmuc());
        return danhMucR.save(dm);
    }
    
    @DeleteMapping("/{id}")
    public boolean DeleteMapping (@PathVariable int id){
        try {
            danhMucR.deleteById(id);
            return true;
        }catch (Exception e) {
            return false;
        }

    }
    

}
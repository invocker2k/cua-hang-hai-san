package com.minishop.Controller;

import java.util.List;
import java.util.Optional;

import com.minishop.Model.SanPham;
import com.minishop.Repository.SanPhamRepository;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/sp")
public class SanPhamController {
    @Autowired 
    SanPhamRepository sanPhamR;

    @GetMapping()
    // @Cacheable("getAllSP")
    public List<SanPham> getAllSP(){
        return sanPhamR.findAll();
    }
    @GetMapping("/dm/{ma_danh_muc}")
    public List<SanPham> getAllSPByMadanhmuc(@PathVariable int ma_danh_muc){
        return sanPhamR.findByMadanhmuc(ma_danh_muc);
    }

    @GetMapping("/pg/{index}")
    public Page<SanPham> get5Sp(@PathVariable int index){
        return sanPhamR.findAll(PageRequest.of(index,5));
    }

     @GetMapping("/{id}")
    //  @Cacheable("getSP")
     public Optional<SanPham> getSP(@PathVariable int id){
                return sanPhamR.findById(id);
     }

    @PostMapping("/save")
    public SanPham addSanPham(@ModelAttribute SanPham sp){
        return sanPhamR.saveAndFlush(sp);
    }


    @PutMapping
    public SanPham repairSanPham(@ModelAttribute SanPham reqSp) {
        SanPham sp = sanPhamR.findById(reqSp.getMasanpham()).orElse(reqSp);
        sp.setGiatien(reqSp.getGiatien());
        sp.setHinhsanpham(reqSp.getHinhsanpham());
        sp.setMota(reqSp.getMota());
        sp.setTensanpham(reqSp.getTensanpham());
        sp.setMadanhmuc(reqSp.getMadanhmuc());
        System.out.println(sp);
        return sanPhamR.save(sp);
    }
    @DeleteMapping("/{id}")
    public boolean xoaSp(@PathVariable("id") int ma){
        try {
            sanPhamR.deleteById(ma);
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
}
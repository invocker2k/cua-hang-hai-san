package com.minishop.Controller;

import java.util.List;
import java.util.Optional;

import com.minishop.Model.ChiTietSanPham;
import com.minishop.Repository.ChiTietSanPhamRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/ctsp")
public class ChiTietSanPhamController {
    @Autowired
    ChiTietSanPhamRepository chitietsanphamR;

    @GetMapping
    public List<ChiTietSanPham> SanPham()
    {
        return chitietsanphamR.findAll();
    }

    @GetMapping("/{id}")
    public Optional<ChiTietSanPham> getsingleChiTietSanPham(@PathVariable int id)
    {
        return chitietsanphamR.findById(id);
    }
    @PostMapping()
    public ChiTietSanPham postMethodName (@RequestBody ChiTietSanPham ct)
    {
        return chitietsanphamR.save(ct);
 
    }
    @PutMapping()
    public ChiTietSanPham putMethodName(@ModelAttribute ChiTietSanPham ctReq)
    {
        ChiTietSanPham ct = chitietsanphamR.findById(ctReq.getId()).orElse(ctReq);
        ct.setId(ctReq.getId());
        ct.setMasanpham(ctReq.getMasanpham());
        ct.setDetail_title(ctReq.getDetail_title());
        return chitietsanphamR.save(ct);
    }
    @DeleteMapping("/{id}")
    public void DeleteMapping(@PathVariable int id)
    {

    } 
}
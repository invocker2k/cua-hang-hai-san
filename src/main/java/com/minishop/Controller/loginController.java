 package com.minishop.Controller;

 import com.minishop.Model.CustomUserDetails;
 import com.minishop.jwt.JwtTokenProvider;
 import com.minishop.payload.LoginRequest;
 import com.minishop.payload.loginResponse;

 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.security.authentication.AuthenticationManager;
 import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
 import org.springframework.security.core.Authentication;
 import org.springframework.security.core.context.SecurityContextHolder;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RestController;
 import org.springframework.web.bind.annotation.PostMapping;
 import org.springframework.web.bind.annotation.RequestBody;


 @RestController
 @RequestMapping("/api")
 public class loginController {
     @Autowired
     AuthenticationManager authenManager;

     @Autowired
     private JwtTokenProvider token;

     @PostMapping(value="login")
     public loginResponse postMethodName( @RequestBody LoginRequest req) {
        
      
         // Xác thực từ username và password.
         Authentication authentication = authenManager.authenticate(new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPass()) );

         // Nếu không xảy ra exception tức là thông tin hợp lệ
         // Set thông tin authentication vào Security Context
         SecurityContextHolder.getContext().setAuthentication(authentication);

         // Trả về jwt cho người dùng.
         String jwt = token.generateToken((CustomUserDetails) authentication.getPrincipal());
         return new loginResponse(jwt);
     }
    
 }
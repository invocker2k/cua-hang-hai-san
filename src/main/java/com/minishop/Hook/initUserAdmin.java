package com.minishop.Hook;

import com.minishop.Model.d_User;
import com.minishop.Repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class initUserAdmin implements ApplicationRunner {
    @Autowired
    UserRepository userR;
    @Autowired
    PasswordEncoder passEncoder;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // TODO Auto-generated method stub
        d_User user = new d_User();
        user.setUsername("invocker");
        user.setPass(passEncoder.encode("invocker"));
        userR.save(user);
    }
    
}
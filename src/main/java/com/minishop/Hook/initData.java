package com.minishop.Hook;
import com.minishop.Model.DanhMucSanPham;
import com.minishop.Model.SanPham;
import com.minishop.Repository.DanhMucRepository;
import com.minishop.Repository.SanPhamRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnResource(resources = "/application-local.properties")
public class initData implements ApplicationRunner{

    @Autowired
    DanhMucRepository danhmucR;
    @Autowired
    SanPhamRepository spR;
    @Override
    public void run(ApplicationArguments arg) throws Exception{
        //dm
        DanhMucSanPham dm = new DanhMucSanPham();
        dm.setId(1);
        dm.setMadanhmuc("C1");
        dm.setTendanhmuc("Cá");
        dm.setHinhdanhmuc("url");
        DanhMucSanPham dm2 = new DanhMucSanPham();
        dm2.setId(2);
        dm2.setMadanhmuc("M1");
        dm2.setTendanhmuc("Tôm");
        dm2.setHinhdanhmuc("url");
        DanhMucSanPham dm3 = new DanhMucSanPham();
        dm3.setId(3);
        dm3.setMadanhmuc("C2");
        dm3.setTendanhmuc("Cua");
        dm3.setHinhdanhmuc("url");
        DanhMucSanPham dm4 = new DanhMucSanPham();
        dm4.setId(4);
        dm4.setMadanhmuc("M1");
        dm4.setTendanhmuc("Mực");
        dm4.setHinhdanhmuc("url");
        DanhMucSanPham dm5 = new DanhMucSanPham();
        dm5.setId(5);
        dm5.setMadanhmuc("H1");
        dm5.setTendanhmuc("Hả sản");
        dm5.setHinhdanhmuc("url");

        danhmucR.save(dm);
        danhmucR.save(dm2);
        danhmucR.save(dm3);
        danhmucR.save(dm4);
        danhmucR.save(dm5);


        //sp
        SanPham sp = new SanPham();
        sp.setMasanpham(1);
        sp.setGiatien("234712");
        sp.setHinhsanpham("tom_3.jpg");
        sp.setMota("mota");
        sp.setTensanpham("Tôm Hải phỏng");
        sp.setMadanhmuc(1);
        SanPham sp2 = new SanPham();
        sp2.setMasanpham(2);
        sp2.setGiatien("234712");
        sp2.setHinhsanpham("tom_3.jpg");
        sp2.setMota("mota");
        sp2.setTensanpham("Mực hạ long tưi tận gốc");
        sp2.setMadanhmuc(1);
        SanPham sp3 = new SanPham();
        sp3.setMasanpham(3);
        sp3.setGiatien("234712");
        sp3.setHinhsanpham("tom_3.jpg");
        sp3.setMota("mota");
        sp3.setTensanpham("Trái đât tròn");
        sp3.setMadanhmuc(1);

        for (int x = 4; x< 50; x++) {
            SanPham spx = new SanPham();
            spx.setMasanpham(x);
            spx.setGiatien(x + "2000");
            if (x < 20) {
                spx.setHinhsanpham("ca_chim.jpg");
            } else if (x < 30) {
                spx.setHinhsanpham("tom_3.jpg");
            }
            else if (x < 40) {
                spx.setHinhsanpham("cua_ng.jpg");
            }else spx.setHinhsanpham("muc_bien.jpg");

            spx.setMota("mota");
            spx.setTensanpham("tensanpham");
            if (x < 20) {
                spx.setMadanhmuc(1);
            } else if (x < 30) {
                spx.setMadanhmuc(2);
            }
            else if (x < 40) {
                spx.setMadanhmuc(3);
            }else spx.setMadanhmuc(4);
            spR.save(spx);
        }

        //save sp
        spR.save(sp);
        spR.save(sp2);
        spR.save(sp3);
    }
}
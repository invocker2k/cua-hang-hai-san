// package com.minishop;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.CommandLineRunner;
// import org.springframework.data.redis.core.RedisTemplate;
// import org.springframework.stereotype.Component;

// @Component
// public class RedisExample implements CommandLineRunner {
//     @Autowired
//     private RedisTemplate template;

//     @Override
//     public void run(String... args) throws Exception {
//         // Set giá trị của key "loda" là "hello redis"
//         template.opsForValue().set("invocker","hello world");

//         // In ra màn hình Giá trị của key "loda" trong Redis
//         System.out.println("Value of key loda: "+template.opsForValue().get("invocker"));
//     }
// }
